.. availability documentation master file, created by
   sphinx-quickstart on Wed Feb  8 13:30:04 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _availability:

availability
============

.. availability:: Unix, not Emscripten, not WASI.

   Hello world I'm the content of the "availability" directive.
