# Update pot file

    make gettext


# Update po file from pot file

    msgmerge --update locale/fr/LC_MESSAGES/index.po build/gettext/index.pot


# Build in english

    make html


# Build in french

    make html SPHINXOPTS="-D locale_dirs=$(pwd)/locale -D language=fr"


# In english it yields

> availability

> Availability: Unix, not Emscripten, not WASI.

> Hello world I’m the content of the “availability” directive.


# While in French the content of the directive is lost

> disponibilité

> Disponibilité: Unix, mais pas Emscripten, ni WASI.
